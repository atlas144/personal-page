+++
title = "O mně"
description = "Informace o mé osobě ve formě +- životopisu."
weight = 2
+++

## Kontakt

- E-mail {{ fd() }} **[jakubsvarc@protonmail.com](mailto:jakubsvarc@protonmail.com)**
- Mastodon {{ fd() }} **[@atlas144@mastodon.arch-linux.cz](https://mastodon.arch-linux.cz/@atlas144)**
- Codeberg {{ fd() }} **[atlas144](https://codeberg.org/atlas144)**

## Vzdělání

{% compact(title="Distribuované výpočetní systémy  - Výpočetní technika", title_pos="left") %}

- *Západočeská univerzita v Plzni, Fakulta aplikovaných věd*
- Navazující magisterské prezenční studium
- **Právě studováno**

{% end %}

{% compact(title="Aplikovaná informatika – Embedded systémy", title_pos="left") %}

- *Jihočeská univerzita v Českých Budějovicích, Přírodovědecká fakulta*
- Bakalářské prezenční studium
- Téma bakalářské práce: **Konstrukce a řízení kolového robotického prostředku**
- Absolvováno s vyznamenáním

{% end %}

{% compact(title="Elektrotechnika – počítačové systémy", title_pos="left") %}

- *SOŠ strojní a elektrotechnická Velešín*
- Maturitní prezenční studium

{% end %}

## Pracovní zkušenosti

{% compact(title="Backend vývojář - junior", title_pos="left") %}

- *Robert Bosch, spol. s r.o.*
- **01/2023 - dosud**
- Vývoj aplikace pro zpracování a ukládání dat z měřících zařízení.
- *Užívané technologie:*
  - Java (JDK 17)
  - Spring
  - Git
  - OpenShift
  - Jenkins
  - Linux (Ubuntu)

{% end %}

{% compact(title="IT podpora", title_pos="left") %}

- *Robert Bosch, spol. s r.o.*
- **06/2021 - 06/2023**
- Správa a instalace softwaru, distribuce a opravy hardwaru, podpora uživatelů.

{% end %}

{% compact(title="Backend vývojář - junior", title_pos="left") %}

- *FIEDLER AMS, s.r.o.*
- **Krátkodobé projekty v období 2018 - 2019 a 2020 - 2021**
- Vývoj menších serverových aplikací pro jednoduché zpracování dat (obrázkové soubory, e-maily).
- *Užívané technologie:*
  - TypeScript
  - Node.js
  - Git

{% end %}

{% compact(title="Frontend vývojář - junior", title_pos="left") %}

- *Jihočeská univerzita v Českých Budějovicích, Přírodovědecká fakulta*
- **06/2022 - 12/2022**
- Refactoring a zjednodušování codebase aplikace pro ukládání vědeckých vzorků ([UniCatDB](https://www.unicatdb.org/)).
- *Užívané technologie:*
  - TypeScript
  - React.js
  - Git

{% end %}

## Certifikáty

{% compact(title="Red Hat Academy", title_pos="left") %}

- Red Hat System Administration I (RH124)

{% end %}

{% compact(title="Cisco Networking Academy", title_pos="left") %}

- CCNA 2 R&S: Routing and Switching Essentials
- CCNA 1 R&S: Introduction to Networks
- NDG Linux Essentials
- Introduction to IoT
- Packet Tracer Know How 1: Packet Tracer 101
- IT Essentials: PC Hardware and Software

{% end %}
