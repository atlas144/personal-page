+++
title = "Cisco IOS základy"
date = 2024-10-22
description = "Přehled základní konfigurace v Cisco IOS."

[extra]

disclaimer = "Tento návod vznikal poněkud ve spěchu, takže je velmi pravděpodobné, že obsahuje chyby. Pokud nějakou naleznete, budu moc rád, když mě o ní [informujete](https://atlas144.codeberg.page/about-me/#kontakt). Děkuji!"
+++

## Pomocné příkazy

`do <prikaz>` - mohu používat příkazy z jiného režimu

`int range f0/1-2` - místo jednoho mohu vybrat rozsah

`$ipcalc <ip adresa site> -s <... pocet hostu pro jednotlive podsite (napr. 10 8 45)>` - (ze stanice) vypíše rozdělení na podsítě podle zadání

## Výpis informací

`Switch#show ip int br` - stav interfaců

`Switch#show mac address-table` - MAC tabulka

`Switch#show vlan` - info o VLAN

`Switch#show int f0/5 switchport` - L2 info o interfacu

`Router#show running-config` - aktuální konfigurace

`Router#show ip route` - routovací tabulka

`Router#show ip ospf database` - info o OSPF

`Router#show ip nat statistics` - info o NAT

`Router#show standby` - info o HSRP

`Router#show vrrp` - info o VRRP

## Základní konfigurace SW

### Nastavení VLAN

Dělám skoro vždy. Většinou na začátku.

```bash
Switch(config)#vlan <cislo VLANy>
Switch(config-vlan)#name <nazev>    #bonusova vec, nejde u VLAN 1
Switch(config-vlan)#exit
Switch(config)#interface vlan <cislo VLANy>
Switch(config-if)#ip add <ip> <maska>
Switch(config-if)#no shutdown
```

#### Umístění portů do VLAN

Pokud dělám VLANy, musím jim přiřadit dané porty.

```bash
Switch(config)#int <nazev interfacu>
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan <cislo VLANy>
```

### Konzole

```bash
Switch(config)#line console 0
Switch(config-line)#loggin synchronous
```

#### Zabezpečení konzole

```bash
Switch(config)#line console 0
Switch(config-line)#password <heslo>
Switch(config-line)#login   #bez toho se heslo nepozaduje
```

### Vzdálený přístup (SSH)

```bash
Switch(config)#line vty 0 15
Switch(config-line)#transport input ssh
Switch(config-line)#login local
Switch(config)#username <jmeno> privilege <hodnota (15)> password <heslo>
Switch(config)#ip domain-name <neco>
Switch(config)#crypto key generate rsa↵
#How many bits in the modulus [512]: 1024 (alespon)
Switch(config)#ip ssh version 2
```

### Bannery

Zprávy při různých akcích.

```bash
Switch(config)#banner incoming c <zprava> c
Switch(config)#banner login c <zprava> c
```

### Uložení konfigurace

```bash
Switch#copy running <nazev souboru>.conf
```

nebo

```bash
Switch#write
```

### Reboot

```bash
Switch#reload
```

### Smazat konfiguraci

```bash
Switch#erase startupconfig
```

## Redundantní spoje na L2

- Vždy musím konfigurovat celé skupiny portů (tam, kde jsou redundantní)
- Mezi SW dávám `switchport mode trunk`

Na SW nastavím pro každou skupinu portů:

```bash
Switch(config)#int range <cisla interfacu>
Switch(config-if-range)#switchport mode trunk
Switch(config-if-range)#channel-group <cislo (1-6)> mode <active/passive>↵
#POZOR - cisla musi byt proti sobe stejna (davam active proti passive)
```

## Směrování

### Mezi VLANami (připojeno vše na jeden RT)

#### Pomocí RT

Nastavit TRUNK na spojení s RT

```bash
Switch(config)#int <nazev interfacu>
Switch(config-if)#switchport mode trunk
```

Virtuální port na RT

```bash
Router(config)#interface <nazev interfacu (napr. g0/0)>.↵
<cislo subinterfacu (idealne cislo VLANy)>
Router(config-subif)#encapsulation dot1Q <cislo VLANy>
Router(config-subif)#ip add <ip z VLANy (obvykle posledni pouzitelna)>↵
<maska VLANy>
```

Na PC nastavit gateway (IP adresa daného subinterfacu na RT)

#### Pomocí SW

**V PT to nejde**

### Statické směrování

**Pozor, u směrování většinou používáme adresu sítě!**

- Používáme sériovou linku mezi RT (červený blesk v PT). V reálu optika nebo něco vysokorychlostního.
- Zadáváme IP sítě, kterou chceme přidat do naší směrovací tabulky.
- Spojení prochází přes přímo připojeného souseda, u kterého zadáváme IP na společné lince.

```bash
Router(config)#int <nazev interfacu>
Router(config-if)#ip add <ip adresa (idealne P2P)> <maska (255.255.255.252)>
Router(config-if)#no shutdown
Router(config-if)#exit
Router(config)#ip route <ip a maska pripojovane VLANy (ta co je na protejsim RT)>↵
<ip linky primo pripojeneho souseda (pres co smerujeme)>
```

#### Nastavení default routy

Standardně ji dávám do internetu.

```bash
Router(config)#ip route 0.0.0.0 0.0.0.0 <ip linky, kam potrebuji>
```

#### Nastavení metriky

Používá se pro záložní cesty (ideálně kolem 200 (dynamické routování dává totiž kolem 100 a obecně ho nechceme předbíhat))

```bash
Router(config)#ip route <ip a maska pripojovane VLANy>↵
<ip linky primo pripojeneho souseda> <metrika (0 - 255)>
```

### Dynamické směrování

#### RIP

Nastavíme, co mají routery inzerovat za sítě.

```bash
Router(config)#router rip
Router(config-router)#version 2
Router(config-router)#network <ip adresa site>
Router(config-router)#default-information originate #generuje default route
```

#### OSPF

- Dělí sít na oblasti. Máme oblast **backbone (AREA 0)**, která slouží k propojování ostatních.

- **ABR (Area Border Router)** - router, přes který se připojuje jiná oblast na AREA 0

##### Nastavení

```bash
Router(config)#router ospf <cislo cehosi (na vsech RT stejne)>
```

Kde mám připojený SW, musím přidat jeho VLAN:

```bash
Router(config-router)#passive-interface g0/0    #smerem ke SW udelam interfacy pasivni
Router(config-router)#passive-interface g0/0.20 #totez pro subinterfacy
Router(config-router)#network <ip site> <reverzni maska> area 0↵
#pridat sit do oblasti (kazdou sit, ktera je k RT pripojena - vcetne te mezi RTs)
```

Na všech routerech:

```bash
Router(config-router)#redistribute connected    #zada pripojene interfacy
```

***NENÍ JISTÉ*** - Pokud nedám *redistribute connected*, tak na všech RT přidám porty, kterých se má OSPF týkat (porty mezi RT):

```bash
Router(config-if)#int <nazev portu>
Router(config-if)#ip ospf 100 area 0
```

Pro default routu (na RT, který má na sobě nastavenou default routu):

```bash
Router(config-router)#default-information originate 
Router(config-router)#redistribute static
```

## DHCP

Automatické přidělování IP adres.

### Na RT

Pokud chci specifikovat, jaké IP nepoužívat (např. IP routeru):

```bash
Router(config)#ip dhcp excluded-addresses <zacatecni IP> <koncova IP>↵
#musim udelat pred poolem!!!
```

Na PC (pokud už má přidělenou adresu ze zakázaného rozsahu):

```bash
$ sudo dhclient -r   #pusti starou
$ sudo dhclient      #vyzada novou
```

Pro každou VLAN, ve které chci používat DHCP:

```bash
Router(config)#ip dhcp pool <nazev>
Router(dhcp-config)#dns-server 8.8.8.8
Router(dhcp-config)#default-router <ip interfacu, ktery funguje jako brana>
Router(dhcp-config)#network <ip site> <maska site | prefix>
```

Výpis tabulky přiřazených adres:

```bash
Router#show ip dhcp binding
```

## NAT

Překlad adres za RT.

Musíme dát LABEL (IN - k LAN, OUT - k ISP), abychom věděli, které int/sub-int překládat.

```bash
Router(config)#int <nazev interfacu>
Router(config-if)#ip nat <in | out>
```

Nastavíme NAT pro celou síť:

```bash
Router(config)#access-list <cislo listu> permit <ip site> <reverzni maska>
Router(config)#ip nat inside source list <cislo listu> interface↵
<cislo interfacu, kam chci prekladat> overload 
```

Můžeme nastavit též pro všechny sítě najednou:

```bash
Router(config)#access-list <cislo listu> permit any
Router(config)#ip nat inside source list <cislo listu> interface↵
<cislo interfacu, kam chci prekladat> overload 
```

Můžeme nastavit též jen pro jeden PC:

```bash
Router(config)#ip nat inside source static <ip pc, kterou chceme prekladat>↵
<ip, na kterou chceme prekladat>
```

## FIRST HOP redundancy

Nastavíme, že jeden RT bude fungovat jako záloha pro druhý, pokud vypadne.

Protokoly - **VRRP** (open source), **HSRP** (Cisco)

### HSRP

#### Postup

1. Nastaví se virtuální IP adresa pro oba RT (nejprve musí mít nastavenou skutečnou - musejí na sebe *ping*nout)
    - zároveň se jim sama nastaví jedna virtuální MAC
2. *DHCP* - RT musí dávat přes DHCP virtuální IP
3. *Preemption* - určuje, zda se má po nahození původního primáru opět stát primárem, či nikoliv (může být nestabilní a výpadek se bude opakovat)
4. *Object tracking (nejde v PT)* - monitoring fungování inerfaců a co dělat při problému s nimi

Na obou RT:

```bash
Router(config)#int g0/0     #k LAN
Router(config-if)#standby ip 192.168.1.250
Router(config-if)#standby priority 50
Router(config-if)#standby preempt   #take mi aktivuje zmenenou prioritu
Router(config-if)#exit
```
Pokud děláme DHCP (a máme již nastaveno):

```bash
Router(config)#ip dhcp pool 1
Router(dhcp-config)#default-router 192.168.1.250
```

A na PC:

```bash
$ sudo dhclient -r   #pusti starou
$ sudo dhclient      #vyzada novou
```

Pokud ještě nemáme, uděláme DHCP jako klasicky, ale default-router bude náš nový a spojený.

### VRRP

Postup je obdobný jako u HSRP.

Nastavení sloučení:

```bash
Router(config)#int <nazev interfacu>
Router(config-if)#vrrp <zvolene cislo sdruzeni (na obou RT stejne)> ip <ip slouceni>
Router(config-if)#vrrp <predesle cislo sdruzeni> priority↵
<cislo (cim vetsi, tim vyssi priorita)>
```

#### Object Tracking

Trackuje nám, jestli nedošlo k selhání interfacu a na základě toho upravuje prioritu RT.

```bash
Router(config)#track <zvolene cislo tracku> int <nazev interfacu, ktery sledujeme>↵
line-protocol
```

Přidáme track k VRRP:

```bash
Router(config)#int <nazev interfacu, kterym jsou RT spojeny (pravdepodobne)>
Router(config-if)#vrrp <predesle cislo sdruzeni> track <predesle cislo tracku>↵
decrement <o kolik chci snizit prioritu>
```

#### IP SLA

Vylepšení *Object Tracking*. Dovoluje mi sledovat selhání i za RT.

- Zadávám IP adresu nějakého serveru v síti. Protokol na ni periodicky posílá ICMP pakety, aby otestoval dostupnost. Pokud není, bere to jako selhání na cestě (což může být chyba - vypadne server, ale zbytek internetu bude přístupný).

```bash
Router(config)#ip sla <zvolene cislo sledovani>
Router(config-ip-sla)#icmp-echo <ip serveru, jehoz pripojeni chci testovat↵
(napr. 8.8.8.8)> source-interface <nazev interfacu, ktery sleduji>
```

Poté musím začít chyby trackovat (pozor na předešlou konfiguraci tracku, nesmím ji mít, jinak nastane kolize):

```bash
Router(config)#track <zvolene cislo tracku> ip sla <predesle cislo sledovani> reachability
Router(config)#ip sla schedule <predesle cislo sledovani> start-time now
```

## Monitoring sítě

### SNMP

**SNMP community** - administrativní doména (podle ní se řídí přístup)

#### Nastavení

```bash
Router(config)#snmp-server community <nazev komunity (zvolim si sam)>↵
<opravneni (napr. ro - read-only)>
```

Dotaz na SNMP info (ze stanice):

```bash
$ snmpwalk -v1 -c <nazev komunity> <ip dotazovaneho oruteru (kde bezi SNMP)>↵
<OID reference, napr. 1.3.6.1.2.1.2.2>
```
