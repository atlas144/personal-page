+++
title = "Jak snížit velikost zkompilovaného programu?"
date = 2023-02-19
description = "Aneb debug a makra preprocesoru na platformě Arduino."
+++

Před nahráním finálního kódu (*produkce*) je dobré zbavit se `Serial.print()` výpisů, které slouží pro **debug**. V případě použití menších MCU, jako např. *ATmega8* je to dokonce mnohdy nutností, jelikož se do nich kód s výpisy nemusí vůbec vejít (není zde problém pouze s načtením do RAM (lze řešit použitím makra `F()`), text se totiž nemusí vejít ani do paměti programu).

Nejsnazší způsob "odstranění" výpisů je pomocí **maker preprocesoru**. Velkou výhodou tohoto přístupu je to, že v případě potřeby debugu v budoucnu nemusíme psát výpisy znovu, stačí nám je pouze zapnout.

## Definice maker

Na začátek souboru (kamkoliv před výkonný kód) přidáme následující definice:

```cpp
#define DEBUG 0

#if DEBUG
	#define serial_begin(x)	    Serial.begin(x)
	#define serial_print(x)	    Serial.print(x)
	#define serial_println(x)   Serial.println(x)
#else
	#define serial_begin(x)
	#define serial_print(x)
	#define serial_println(x)
#endif
```

Pokud bychom chtěli využít i další funkce knihovny `Serial`, definujeme si je ve stejném stylu.

## Použití

Následně můžeme makry nahradit všechna volání v kódu, např.:

```cpp
void setup() {
	Serial.println("Hello world");
	// nahradime za
	serial_println("Hello world");
}
```

Nyní lze mezi použitím a vynecháním jednoduše přepínat pomocí hodnoty `DEBUG` - `0` vypnuto, `1` zapnuto.

## `SoftwareSerial` dodatek

Pro použití se `SoftwareSerial` je třeba přidat dvě dodatečná makra a lehce upravit zbytek:

```cpp
#define SW_DEBUG 0

#if SW_DEBUG
	// nova makra
	#include <SoftwareSerial.h>
	#define sw_serial_def(rx, tx)	SoftwareSerial swSerial(rx, tx)
	
	#define sw_serial_begin(x)	swSerial.begin(x)
	#define sw_serial_print(x)	swSerial.print(x)
	#define sw_serial_println(x)	swSerial.println(x)
#else
	#define sw_serial_def(rx, tx)
	#define sw_serial_begin(x)
	#define sw_serial_print(x)
	#define sw_serial_println(x)
#endif
```

## Zdroje

- ["NullSerial: reducing production code size" na Arduino fóru](https://forum.arduino.cc/t/nullserial-reducing-production-code-size/37037/2)
- Konvence názvů maker převzata z: HEROUT, Pavel. *Učebnice jazyka C*. České Budějovice: Kopp, 2018. ISBN 978-80-7232-383-8.
