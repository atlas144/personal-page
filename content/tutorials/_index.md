+++
title = "Návody & Informace"
description = "Vzdělávací materiály, návody a další informace posbírané po různých zákoutích internetu či ve školních učebnicích a skriptech. Převážně o programování, Linuxu, svobodě na internetu, robotice a elektronice."
weight = 1
sort_by = "date"
+++