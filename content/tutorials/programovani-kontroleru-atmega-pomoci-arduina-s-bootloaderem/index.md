+++
title = "Programování kontrolérů ATmega pomocí Arduina (s bootloaderem)"
date = 2022-03-11
description = "Jednoduchý způsob, jak programovat samostatné ATmega kontroléry s minimálním počtem potřebných komponent."
+++

## Potřebný HW a SW

### HW

- ATmega kontrolér, který budeme programovat ([seznam podporovaných](https://github.com/MCUdude/MiniCore#supported-microcontrollers))
- Arduino UNO (nebo jiné), pomocí kterého budeme programovat
- krystal 16 MHz (není nutný)
- 2x 22 pF (bez krystalu nejsou nutné)
- 10 kΩ (mělo by fungovat cokoliv mezi 1 kΩ a 20 kΩ)

### SW

- Arduino IDE
- [MiniCore](https://github.com/MCUdude/MiniCore)

## Instalace jádra

Přidáme následující odkaz do **Soubor > Vlastnosti > Správce dalších desek URL**: `https://mcudude.github.io/MiniCore/package_MCUdude_MiniCore_index.json` (aktuální odkaz [zde](https://github.com/MCUdude/MiniCore#boards-manager-installation)). Poté otevřeme správce desek: **Nástroje > Vývojová deska: x > Manažer Desek...**, vyhledáme *MiniCore* a nainstalujeme jej.

## Příprava Arduina

Abychom mohli pomocí Arduina programovat jiné kontroléry, musíme do něj nahrát ISP sketch: **Soubor > Příklady > 11.ArduinoISP > ArduinoISP**.

## Zapojení

![Zapojení](./zapojeni.png)

## Vypálení bootloaderu

Nastavíme správnou desku (=kontrolér, který chceme programovat): **Nástroje > Vývojová deska: *x* > MiniCore > ATmega*x***, zvolíme použitý oscilátor: **Nástroje > Clock** (zde *External 16 MHz*, pokud nemáme externí, použijeme *Internal 8 MHz*), nastavíme programátor: **Nástroje > Programátor > Arduino as ISP (MiniCore)** a vypálíme bootloader: **Nástroje > Vypálit zavaděč**.

## Nahrání programu

Otevřeme sketch, který chceme nahrát, nastavíme parametry (stejně jako v sekci ***[Vypálení bootloaderu](#vypaleni-bootloaderu)*** a sketch nahrajeme: **Projekt > Nahrát pomocí programátoru**. Zapojení zůstává stále stejné.

## Zdroje

- [MiniCore](https://github.com/MCUdude/MiniCore)
- [Programming ATmega8 Using Arduino IDE](https://create.arduino.cc/projecthub/hami/programming-atmega8-using-arduino-ide-90c2ad)
